# TI-Nspire Screenshot Cli
Have you ever tried to grab a screenshot form your TI-Nspire handheld to do something with it but failed because you haven't found a way to do this in a script? This is the Software for you!
## Usage
TIScreen is really simple to use. There are two commandline options.

`TIScreen filename [Port]`

Please note that the file outputted will always be in the png format. This might change in the future.
Port is the number of the port to probe on. It uses the TILP enumeration.
## dependencies
TIScreen currently depends on:
  * `ticables2 >= 1.3.0`
  * `ticalcs2 >= 1.1.8`
  * `tifiles2 >= 1.1.6`
  * `ticonv >= 1.1.4`
  * `glib-2.0 >= 2.6.0`
  * `gdk-pixbuf-2.0 >= 2.12.0`

Building TIScreen further requires:
  * `automake`
  * `autoconf`
  * `make`
  * `gcc`

## Build
 1. Install Dependencies. 
    On Debian-based OSs run:

    `sudo apt install automake autoconf make gcc libticables-dev libticalcs-dev libtifiles-dev libticonv-dev libgdk-pixbuf2.0-dev libglib2.0-dev`
    
    On Arch-Based Systems run:
    
    `sudo pacman -S automake autoconf make gcc gdk-pixbuf2 glib2` **Note:** You need to install `ticables2, ticalcs2, tifiles2, ticonv` from the AUR or other sources
  2. Prepare the build environment:

     ```
     aclocal
     autoheader
     automake --add-missing
     autoconf
     ./configure
     ```
   3. Build using `make`
   
## Thanks
Special Thanks to the [TILP Team](https://github.com/debrouxl/tilp)
   
## License
TIScreen is Copyright (C) 2018  n1kPLV

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

