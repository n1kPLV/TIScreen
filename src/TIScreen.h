/*
 * TIScreen - Screenshots from Nspire
 * Copyright (C) 2018  n1kPLV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "tilibs.h"

typedef struct 
{
    uint8_t* bitmap;
    int      width;
    int      height;
} TilpScreen;

uint8_t* screen_gs_convert(CalcScreenCoord screen, guchar *bitmap);

uint8_t* screen_16bitcolor_convert(CalcScreenCoord screen, guchar *bitmap);