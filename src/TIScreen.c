/*
 * TIScreen - Screenshots from Nspire
 * Copyright (C) 2018  n1kPLV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gdk-pixbuf/gdk-pixbuf-core.h>
#include "tilibs.h"
#include "TIScreen.h"
#include <stdio.h>
#include <glib-2.0/glib.h>
#include <ctype.h>
#include <stdbool.h>

void destroy_pixbuf(guchar * pixels, gpointer data)
{
	g_free(pixels);
}

int main(int argc, char** argv)
{
    printf("[DEBUG] Arguments passed: %s\n\t%s\n\t%d\n", argv[1], argv[2], argc);
    GdkPixbuf *pixbuf;
    int err;
    //Init
    CableModel cbm;
    CablePort cbp;
    CalcModel cm;
    const gchar *type;
    const gchar *filename;
    GError *error = NULL;
    gint h,w;
		printf("I'm here! DEBUG POINT 0\n");

    ticables_library_init();
    ticalcs_library_init();

    //Arg Processing
		//let the compiler be happy :D
		cbp = PORT_1;
		filename = "/dev/null";
    if (argc <= 1){
        fprintf(stderr, "%s\n", "[ERROR] No Filepath given");
				return -1;
    } else if (argc == 2) {
        filename = argv[1];
        cbp = PORT_1;
    } else if (argc == 3) {
        filename = argv[1];
        switch (atoi(argv[2])) {
            case 0:
                cbp = PORT_0;
                printf("[DEBUG] Using Port 0\n");
                break;
            case 1:
                cbp = PORT_1;
                break;
            case 2:
                cbp = PORT_2;
                break;
            case 3:
                cbp = PORT_3;
                break;
            case 4:
                cbp = PORT_4;
                break;
            default:
                cbp = PORT_1;
        }
    }

    cbm = CABLE_USB;
    cm = CALC_NSPIRE;

    CableHandle* cable = ticables_handle_new(cbm,cbp);
    CalcHandle* calc = ticalcs_handle_new(cm);
    ticalcs_cable_attach(calc, cable);
		ticables_options_set_timeout(cable, 10);
		printf("I'm here! DEBUG POINT 1\n");
    if (ticalcs_calc_isready(calc)) {
        fprintf(stderr, "%s\n", "[ERROR] Calc is not ready");
        return -111;
    }

		printf("I'm here! DEBUG POINT 2\n");
    CalcScreenCoord screen;

		guchar *bitmap = g_malloc(3*320*240);
    screen.format = SCREEN_FULL;
    err = ticalcs_calc_recv_screen(calc, &screen, &bitmap);
		printf("I'm here! DEBUG POINT 3\n");
    fprintf(stderr, "screen recieve err: %d\n", err);
		w = screen.width;
		h = screen.height;
    uint8_t *bytemap;

    // For Nspires, we have to determine the calc model...
		CalcInfos infos;
		if (ticalcs_calc_get_version(calc, &infos))
		{
			return -10;
		}
		if (infos.bits_per_pixel == 4)
		{
			// Nspire (CAS) Clickpad or Touchpad.
            printf("[DEBUG] Found a Nspire (CAS) Clickpad or Touchpad\n");
			bytemap = screen_gs_convert(screen, bitmap);
		}
		else if (infos.bits_per_pixel == 16)
		{
			// Nspire (CAS) CX.
            printf("[DEBUG] Found a Nspire (CAS) CX\n");
			bytemap = screen_16bitcolor_convert(screen, bitmap);
		}
		else
		{
			fprintf(stderr, "%s %d\n", "[ERROR] Unknown calculator model with bpp", infos.bits_per_pixel);
			return -12;
		}
		printf("[DEBUG] Screen width: %d; Screen height: %d\n", w, h);

    pixbuf = gdk_pixbuf_new_from_data(bytemap, GDK_COLORSPACE_RGB, false, 8, w, h, 3 * w, destroy_pixbuf, NULL);

    type = "png";

    printf("[DEBUG] Filename to save to: %s\n", filename);
    printf("[DEBUG] %d\n",gdk_pixbuf_save(pixbuf, filename, type, &error, NULL));

		ticalcs_cable_detach(calc);
    ticables_cable_close(cable);
    ticalcs_handle_del(calc); calc = NULL;
    ticables_handle_del(cable); cable = NULL;
    return 0;
}

//Following code used under License from TILP

/*
  Convert an NSpire grayscale bitmap into a bytemap.
  The returned RRGGBB array must be freed when no longer used.
*/

uint8_t *screen_gs_convert(CalcScreenCoord screen, guchar *bitmap)
{
    gint w = screen.width;
    gint h = screen.height;
    guchar *bytemap = (guchar*) g_malloc(3 * w * h);
    int i, j;
    for (i = 0; i < h; i++)
    {
        for (j = 0; j < w / 2; j++)
        {
            uint8_t data = bitmap[(w / 2) * i + j];
            uint8_t lo = data & 0x0f;
            uint8_t hi = data >> 4;
            int pos = w * i + 2 * j;
            bytemap[3 * pos + 0] = hi << 4;
            bytemap[3 * pos + 1] = hi << 4;
            bytemap[3 * pos + 2] = hi << 4;
            bytemap[3 * pos + 3] = lo << 4;
            bytemap[3 * pos + 4] = lo << 4;
            bytemap[3 * pos + 5] = lo << 4;
        }
    }
    return bytemap;
}

/*
  Convert an NSpire CX 16-bit color bitmap into a bytemap.
  The returned RRGGBB array must be freed when no longer used.
*/

uint8_t *screen_16bitcolor_convert(CalcScreenCoord screen, guchar *bitmap)
{
    gint w = screen.width;
    gint h = screen.height;
    guchar *bytemap = (guchar*) g_malloc(3 * w * h);
    int i, j;
    for (i = 0; i < h; i++)
    {
        for (j = 0; j < w; j++)
        {
            uint16_t data = ((uint16_t *)bitmap)[w * i + j];
            int pos = w * i + j;
            // R5 G6 B5.
            bytemap[3 * pos + 0] = ((data & 0xF800) >> 11) << 3;
            bytemap[3 * pos + 1] = ((data & 0x07E0) >> 5) << 2;
            bytemap[3 * pos + 2] = ((data & 0x001F) >> 0) << 3;
        }
    }
    return bytemap;
}
